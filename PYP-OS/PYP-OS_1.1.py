import time as t
print("Loading...")
t.sleep(2)
ram = 32
rom = 128
user = "Guest"
operationsystem = "PYP-OS"
print("Ram:", ram, "mb")
print("Rom:", rom, "mb")
print("Import library 'time' ...")
t.sleep(1)
print("Starting PYP-OS...")
t.sleep(2)
print("Welcome!")
print("Write 'information' for help.")

while True:
    print(">>>", end="")
    command = input()
    if command == "information":
        print("For start program write 'cd' and 'name_program'\n1. calculator \n2. infosystem\n3. explorer")
    elif command == "cd calculator":     
        print("Stupid Calculator 1.0.py")
        print("One number:", end="")
        on = input()
        print("Two number:", end="")
        tn = input()
        print("Plus or Minus:", end="")
        guess = input()
        while True:
            if guess == "plus" or "Plus":
                print("Answer:", float(on) + float(tn))
                break
            if guess == "minus" or "Minus":
                print("Answer:", float(on) - float(tn))
                break
            else:
                print("Incorrect (Plus or Minus)!")
                continue
    elif command == "cd infosystem":
        print("Infosystem.py")
        print("Ram:", ram, "mb")
        print("Rom:", rom, "mb")
        print("User's name:", user)
        print("Operation system's name:", operationsystem)
    elif command == "cd explorer":
        print("Explorer.py")
        print("FLP:\Files\:")
        print("time.py    LIBRARY    28.11.2022")
        print("settings.py    PYTHON    27.11.2022")
        print("PYP-OS.py    SYSTEM    27.11.2022")
        print("PYP-OS-OLD.py    SYSTEMOLD    01.08.2022")
    else:
        print("Incorrect!")
